#!/bin/bash

# Make sure that the script receives the file path as an argument
if [ $# -ne 1 ]; then
    echo "Usage: $0 <file_path>"
    exit 1
fi

file="$1"

# Check for non-ascii characters
if LC_ALL=C sudo grep -q '[^ -~]' "$file"; then
    echo "$file"
    exit 0
fi

# Count lines, words, and characters without actually reading the file contents
lines=$(sudo awk 'END { if (NR > 0) print NR }' "$file" 2>/dev/null)
words=$(sudo awk '{ total += NF } END { if (total > 0) print total }' "$file" 2>/dev/null)
chars=$(sudo awk '{ total += length } END { if (total > 0) print total }' "$file" 2>/dev/null)

# Set lines, words, and chars to 0 if they are empty
lines=${lines:-0}
words=${words:-0}
chars=${chars:-0}

# Check if lines, words, and chars are greater than specified thresholds
if [ "$lines" -lt 3 ] && [ "$words" -gt 1000 ] && [ "$chars" -gt 2000 ]; then
    echo "$file"  # Print the name of the potentially dangerous file
    exit 0
fi

# Check for keywords in the file
if sudo grep -q -e "corrupted" -e "dangerous" -e "risk" -e "attack" -e "malware" -e "malicious" "$file"; then
    echo "$file"  # Print the name of the potentially dangerous file
fi
