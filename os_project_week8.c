#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>

typedef struct {
    char entry[4096];
    char timestamp[32]; 
    long size;
    char last_modified[32]; 
    mode_t permissions;
    ino_t inode_no;
    char type[32];
} FileMetadata;

int compare_metadata(const FileMetadata *old_meta, const FileMetadata *new_meta) {
    if (strcmp(old_meta->entry, new_meta->entry) != 0)
        return 0;
    if (strcmp(old_meta->timestamp, new_meta->timestamp) != 0)
        return 0;
    if (old_meta->size != new_meta->size)
        return 0;
    if (strcmp(old_meta->last_modified, new_meta->last_modified) != 0)
        return 0;
    if (old_meta->permissions != new_meta->permissions)
        return 0;
    if (old_meta->inode_no != new_meta->inode_no)
        return 0;
    if (strcmp(old_meta->type, new_meta->type) != 0)
        return 0;
    return 1;
}

void read_snapshot(const char *snapshot_filename, FileMetadata *metadata_array, int *count_entries) {
    FILE *snapshot_file = fopen(snapshot_filename, "r");
    if (snapshot_file == NULL) {
        perror("fopen");
        exit(1);
    }

    *count_entries = 0;
    while (fscanf(snapshot_file, "Entry: %[^\n]\n", metadata_array[*count_entries].entry) != EOF) {
        fscanf(snapshot_file, "Timestamp: %[^\n]\n", metadata_array[*count_entries].timestamp);
        fscanf(snapshot_file, "Size: %ld bytes\n", &metadata_array[*count_entries].size);
        fscanf(snapshot_file, "Last Modified: %[^\n]\n", metadata_array[*count_entries].last_modified);
        fscanf(snapshot_file, "Permissions: %o\n", &metadata_array[*count_entries].permissions);
        fscanf(snapshot_file, "Inode no: %ld\n", &metadata_array[*count_entries].inode_no);
        fscanf(snapshot_file, "Type: %[^\n]\n", metadata_array[*count_entries].type);
        (*count_entries)++;
    }

    fclose(snapshot_file);
}

void write_snapshot(const char *snapshot_filename, const FileMetadata *metadata_array, int count_entries) {
    FILE *snapshot_file = fopen(snapshot_filename, "w");
    if (snapshot_file == NULL) {
        perror("fopen");
        exit(1);
    }

    for (int i = 0; i < count_entries; i++) {
        fprintf(snapshot_file, "Entry: %s\n", metadata_array[i].entry);
        fprintf(snapshot_file, "Timestamp: %s\n", metadata_array[i].timestamp);
        fprintf(snapshot_file, "Size: %ld bytes\n", metadata_array[i].size);
        fprintf(snapshot_file, "Last Modified: %s\n", metadata_array[i].last_modified);
        fprintf(snapshot_file, "Permissions: %o\n", metadata_array[i].permissions);
        fprintf(snapshot_file, "Inode no: %ld\n", metadata_array[i].inode_no);
        fprintf(snapshot_file, "Type: %s\n", metadata_array[i].type);
        fprintf(snapshot_file, "\n");
    }

    fclose(snapshot_file);
}

void traverse_directory(const char *dir_path, const char *output_dir) {
    DIR *dir;
    struct dirent *entry;
    struct stat file_info;
    char filepath[4096];
    struct tm *tm_info;

    dir = opendir(dir_path);
    if (dir == NULL) {
        perror("opendir");
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue; 

        sprintf(filepath, "%s/%s", dir_path, entry->d_name);

        if (stat(filepath, &file_info) == -1) {
            perror("stat");
            continue;
        }

        tm_info = localtime(&file_info.st_mtime);

        FileMetadata new_meta;
        strcpy(new_meta.entry, entry->d_name);
        sprintf(new_meta.timestamp, "%d-%02d-%02d %02d:%02d:%02d",
                tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec);
        new_meta.size = file_info.st_size;
        sprintf(new_meta.last_modified, "%d-%02d-%02d %02d:%02d:%02d",
                tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec);
        new_meta.permissions = file_info.st_mode & 0777;
        new_meta.inode_no = file_info.st_ino;
        strcpy(new_meta.type, S_ISDIR(file_info.st_mode) ? "Directory" : "File");

        char snapshot_filename[4096];
        snprintf(snapshot_filename, sizeof(snapshot_filename), "%s/%s_snapshot.txt", output_dir, entry->d_name);

        FileMetadata old_meta;
        int count_entries = 0;
        read_snapshot(snapshot_filename, &old_meta, &count_entries);

        if (count_entries == 0 || !compare_metadata(&old_meta, &new_meta)) {
            write_snapshot(snapshot_filename, &new_meta, 1);
        }

        if (S_ISDIR(file_info.st_mode)) {
            traverse_directory(filepath, output_dir); 
        }
    }

    closedir(dir);
}

int main(int argc, char *argv[]) {
    if (argc < 4 || argc > 13 || strcmp(argv[1], "-o") != 0) {
        printf("too many/few arguments\n");
        return 1;
    }

    char *output_dir = argv[2];
    if (access(output_dir, F_OK) == -1) {
        printf("the output directory does not exist\n");
        return 1;
    }

    int num_processes = argc - 3;
    pid_t child_pids[num_processes];

    for (int i = 3; i < argc; i++) {
        pid_t pid = fork();
        if (pid == -1) {
            perror("fork");
            exit(1);
        } else if (pid == 0) { 
            traverse_directory(argv[i], output_dir);
            exit(0);
        } else { 
            child_pids[i - 3] = pid;
        }
    }

    int status;
    for (int i = 0; i < num_processes; i++) {
        pid_t child_pid = wait(&status);
        if (child_pid == -1) {
            perror("wait");
        } else {
            if (WIFEXITED(status)) {
                printf("child process with PID %d was terminated successfully with exit code %d\n", child_pid, WEXITSTATUS(status));
            } else {
                printf("child process with PID %d was NOT terminated successfully\n", child_pid);
            }
        }
    }

    return 0;
}


