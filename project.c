#include <stdio.h> 
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <errno.h> 

//typedef struct to store the metadata of a file
typedef struct {
    char entry[4096]; //name of the file or directory
    char timestamp[32]; //timestamp is stored as a string in a format like 
                        //"YYYY-MM-DD HH:MM:SS"
    long size; //the size of the file in bytes
    char last_modified[32]; //same as timestamp
    mode_t permissions; //the permissions of the file
    ino_t inode_no; //the inode number of the file
    char type[32]; //the type of the file (regular file or directory)
} FileMetadata;

//the following function is used to compare two instances of the
//FileMetadata struct to determine weather they're identical or not
int compare_metadata(const FileMetadata *old_meta, const FileMetadata *new_meta) {
    if (strcmp(old_meta->entry, new_meta->entry) != 0)
        return 0; //metadata is different
    if (strcmp(old_meta->timestamp, new_meta->timestamp) != 0)
        return 0;
    if (old_meta->size != new_meta->size)
        return 0;
    if (strcmp(old_meta->last_modified, new_meta->last_modified) != 0)
        return 0;
    if (old_meta->permissions != new_meta->permissions)
        return 0;
    if (old_meta->inode_no != new_meta->inode_no)
        return 0;
    if (strcmp(old_meta->type, new_meta->type) != 0)
        return 0;
    return 1; //metadata is identical
}

//this function reads metadata from a snapshot file and populates a
//FileMetadata structure with the extracted information
void read_snapshot(const char *snapshot_filename, FileMetadata *metadata, int *count_entries) {
    //open the file (in this case, reading mode)
    int snapshot_file = open(snapshot_filename, O_RDONLY);
    if (snapshot_file == -1) { //if the file opening failed
        if (errno != ENOENT) { //the file does indeed exist, there's another problem
            perror("open");  //indicates that the error occured while trying to open the file
            exit(1); //exit != 1 => the program encountered an error and didn't execute successfully
        }
        *count_entries = 0; //no snapshots
        return; //the file does NOT exist
    }

    *count_entries = 0; //no snapshots
    char buffer[4096]; //to store the data read from the file
    if (read(snapshot_file, buffer, sizeof(buffer)) > 0) {
        sscanf(buffer, "Entry: %[^\n]\n", metadata[*count_entries].entry);
        sscanf(buffer, "Timestamp: %[^\n]\n", metadata[*count_entries].timestamp);
        sscanf(buffer, "Size: %ld bytes\n", &metadata[*count_entries].size);
        sscanf(buffer, "Last Modified: %[^\n]\n", metadata[*count_entries].last_modified);
        sscanf(buffer, "Permissions: %o\n", &metadata[*count_entries].permissions);
        sscanf(buffer, "Inode no: %ld\n", &metadata[*count_entries].inode_no);
        sscanf(buffer, "Type: %[^\n]\n", metadata[*count_entries].type);
        (*count_entries)++;
    }

    close(snapshot_file); 
}

void write_snapshot(const char *snapshot_filename, const FileMetadata *metadata, int count_entries) {
    //the open system call opens the specified snapshot file 
    //in write-only mode (O_WRONLY). If the file does not exist, it 
    //creates it (O_CREAT), and if it already exists, it truncates 
    //its size to 0 (O_TRUNC). The third argument specifies file 
    //permissions for the newly created file 
    //(S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH), 
    //granting read and write permissions to the file owner, group, and others
    int snapshot_file = open(snapshot_filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if (snapshot_file == -1) { //if the file opening failed
        perror("open"); //indicates that the error occured while trying to open the file
        exit(1); //exit != 1 => the program encountered an error and didn't execute successfully
    }

    for (int i = 0; i < count_entries; i++) {
        dprintf(snapshot_file, "Entry: %s\n", metadata[i].entry);
        dprintf(snapshot_file, "Timestamp: %s\n", metadata[i].timestamp);
        dprintf(snapshot_file, "Size: %ld bytes\n", metadata[i].size);
        dprintf(snapshot_file, "Last Modified: %s\n", metadata[i].last_modified);
        dprintf(snapshot_file, "Permissions: %o\n", metadata[i].permissions);
        dprintf(snapshot_file, "Inode no: %ld\n", metadata[i].inode_no);
        dprintf(snapshot_file, "Type: %s\n", metadata[i].type);
        dprintf(snapshot_file, "\n");
    }

    close(snapshot_file);
}

//pipe_write is an integer variable representing a file descriptor 
//used for writing to a pipe. In the context of this program it
//serves as the write end of a pipe, which is a unidirectional communication channel 
//(used to write data (information about potentially dangerous files) to the pipe, 
//which can be read by another process)

void traverse_directory(const char *dir_path, const char *output_dir, const char *isolated_space_dir, int pipe_write, int *dangerous_files_count, int *count_directories) {
    DIR *dir; 
    struct dirent *entry; 
    struct stat file_info;
    char filepath[4096]; 
    struct tm *tm_info; 

    //open the directory specified by dir_path
    dir = opendir(dir_path);
    if (dir == NULL) {
        perror("opendir");
        return;
    }

    //traverse each directory until there are no more directory entries
    while ((entry = readdir(dir)) != NULL) {
        //special directory entries "." and ".." are skipped
        //to avoid infinite recursion and unnecessary processing
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        //the file path for the current directory entry is constructed 
        //by concatenating dir_path and the name of the entry
        sprintf(filepath, "%s/%s", dir_path, entry->d_name);

        //the metadata for the current file is obtained using the stat function
        if (stat(filepath, &file_info) == -1) {
            perror("stat");
            continue;
        }

        //check the file permissions. If there are no permissions 
        //then the file may be dangerous and needs further examination
        if ((file_info.st_mode & (S_IRWXU | S_IRWXG | S_IRWXO)) == 0) { //the file may be dangerous
            //create a pipe to ensure communication between the parent and the child
            int pipe_pc[2];
            if (pipe(pipe_pc) == -1) {
                perror("pipe");
                continue;
            }

            //the child process is forked to execute the shell script 
            pid_t pid = fork();
            if (pid == -1) { //an error occurred during the creation of the childchild
                perror("fork");
                continue;
            } else if (pid == 0) { //the current process is the child process
                //close the end of the pipe that is not used,
                //in this case the read end 
                close(pipe_pc[0]);  

                //the output written to the standard output in the 
                //child process will be written to the pipe instead
                dup2(pipe_pc[1], STDOUT_FILENO); 
                close(pipe_pc[1]);  
                //execute the script (i gave the relative path of the script)
                execl("/bin/bash", "bash", "./verify_for_malicious.sh", filepath, NULL);
                perror("execl");
                exit(EXIT_FAILURE);
            } else { //the ID of the child process in the parent process
                //close the end of the pipe that isn't used (the write end in this case)
                close(pipe_pc[1]);  
 
                char buffer[4096]; //this buffer is used to store data read from the pipe
                ssize_t num_read = read(pipe_pc[0], buffer, sizeof(buffer)); 
                if (num_read > 0) { //data was read from the pipe
                    buffer[num_read - 1] = '\0'; 
                    //pipe_write is used to communicate potentially dangerous 
                    //files from child processes to the parent process
                    write(pipe_write, buffer, num_read);
                    (*dangerous_files_count)++; 
                }

                close(pipe_pc[0]);  
            }
        } else { //the file is not dangerous
            //retrieves the last modification time (st_mtime) of the 
            //file from the file_info structure obtained using the stat function
            tm_info = localtime(&file_info.st_mtime);

            //a FileMetadata structure to hold metadata 
            //information about the current file or directory
            FileMetadata new_meta;
            strcpy(new_meta.entry, entry->d_name);
            sprintf(new_meta.timestamp, "%d-%02d-%02d %02d:%02d:%02d",
                    tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
                    tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec);
            new_meta.size = file_info.st_size;
            sprintf(new_meta.last_modified, "%d-%02d-%02d %02d:%02d:%02d",
                    tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
                    tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec);
            new_meta.permissions = file_info.st_mode & 0777;
            new_meta.inode_no = file_info.st_ino;
            strcpy(new_meta.type, S_ISDIR(file_info.st_mode) ? "Directory" : "File");

            //constructs the filename for the snapshot file 
            //corresponding to the current file or directory
            char snapshot_filename[4096];
            snprintf(snapshot_filename, sizeof(snapshot_filename), "%s/%s_snapshot.txt", output_dir, entry->d_name);

            //a FileMetadata structure to hold metadata from the snapshot
            FileMetadata old_meta;
            int count_entries = 0;
            read_snapshot(snapshot_filename, &old_meta, &count_entries);

            //update the snapshot if either the snapshot file does not exist 
            //(count_entries == 0) or if the metadata of the current file or 
            //directory differs from the existing metadata 
            //(!compare_metadata(&old_meta, &new_meta))
            if (count_entries == 0 || !compare_metadata(&old_meta, &new_meta)) {
                write_snapshot(snapshot_filename, &new_meta, 1);
            }

            //recursive call to make sure the subdirectories are traversed too
            if (S_ISDIR(file_info.st_mode)) {
                traverse_directory(filepath, output_dir, isolated_space_dir, pipe_write, dangerous_files_count, count_directories);
                (*count_directories)++;
            }
        }
    }

    closedir(dir);
}

int main(int argc, char *argv[]) {
    //checks wether the command line arguments are written correctly or not   
    if (argc < 5 || argc > 15 || strcmp(argv[1], "-o") != 0 || strcmp(argv[3], "-s") != 0) {
        printf("Usage: %s -o <output_dir> -s <isolated_space_dir> <dir1> <dir2> ... <dirN>\n", argv[0]);
        return 1;
    }

    //checks wehter the output directory exists
    char *output_dir = argv[2];
    if (access(output_dir, F_OK) == -1) {
        printf("The output directory does not exist.\n");
        return 1;
    }

    //checks wehter the isolated space directory exists
    char *isolated_space_dir = argv[4];
    if (access(isolated_space_dir, F_OK) == -1) {
        printf("The isolated space directory does not exist.\n");
        return 1;
    }

    //create a child process for each directory that has to be traversed

    //create a pipe to ensure communication between the parent and the child
    int pipe_pc[2];
    if (pipe(pipe_pc) == -1) {
        perror("pipe");
        return 1;
    }

    int dangerous_files_count = 0;
    int count_directories = 0;
    for (int i = 5; i < argc; i++) {
        pid_t pid = fork();
        if (pid == -1) {  //an error occurred during the creation of the child
            perror("fork");
            exit(1);
        } else if (pid == 0) { //the current process is the child process
            //close the end of the pipe that is not used,
            //in this case the read end
            close(pipe_pc[0]);  
            //traverse the directory
            traverse_directory(argv[i], output_dir, isolated_space_dir, pipe_pc[1], &dangerous_files_count, &count_directories);
            close(pipe_pc[1]);  
            //terminate and return an exit status to the parent process
            exit(dangerous_files_count); 
        } 
    }
    
    close(pipe_pc[1]);  

    //the buffer is used to store data read from the pipe
    char buffer[4096];
    ssize_t num_read;
    //num_read > 0 indicates that the data was successfully read
    //the loop continues as long as data can be read from the pipe
    while ((num_read = read(pipe_pc[0], buffer, sizeof(buffer))) > 0) { 
        pid_t child_pid = fork();
        if (child_pid == -1) { //an error occurred during the creation of the child
            perror("fork");
            continue;
        } else if (child_pid == 0) { //the current process is the child process
            char corrupted_file[4096]; //the buffer is used to store the name 
                                       //of the corrupted file read from the pipe
            strncpy(corrupted_file, buffer, num_read);
            corrupted_file[num_read - 1] = '\0'; 

            //construct an array of strings args, which will 
            //be used as arguments to the execvp function
            char *args[] = {"/bin/mv", corrupted_file, isolated_space_dir, NULL};
            execvp(args[0], args);
            perror("execvp");
            exit(EXIT_FAILURE);
        } else {
            printf("Moved corrupted file: %s\n", buffer);
        }
    }

    //close the read end of the pipe after all data has been read
    close(pipe_pc[0]);  

    int num_processes = argc - 5;
    int status; //this variable will store the exit status of the child process
    for (int i = 0; i < num_processes; i++) {
        pid_t child_pid = wait(&status); //wait for a child process to terminate and retrieves its process ID
        if (child_pid == -1) {
            perror("wait"); //an error occurred while waiting for the child process
        } else {
            if (WIFEXITED(status)) { //waitingfor the child process was successful
                printf("Child process number %d with PID %d was terminated successfully with %d potentially dangerous files\n", i + 1, child_pid, WEXITSTATUS(status));
            } else {
                printf("Child process number %d with PID %d was NOT terminated successfully\n", i + 1, child_pid);
            }
        }
    }

    return 0;
}
