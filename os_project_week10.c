#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <errno.h> 

typedef struct {
    char entry[4096];
    char timestamp[32];
    long size;
    char last_modified[32];
    mode_t permissions;
    ino_t inode_no;
    char type[32];
} FileMetadata;

int compare_metadata(const FileMetadata *old_meta, const FileMetadata *new_meta) {
    if (strcmp(old_meta->entry, new_meta->entry) != 0)
        return 0;
    if (strcmp(old_meta->timestamp, new_meta->timestamp) != 0)
        return 0;
    if (old_meta->size != new_meta->size)
        return 0;
    if (strcmp(old_meta->last_modified, new_meta->last_modified) != 0)
        return 0;
    if (old_meta->permissions != new_meta->permissions)
        return 0;
    if (old_meta->inode_no != new_meta->inode_no)
        return 0;
    if (strcmp(old_meta->type, new_meta->type) != 0)
        return 0;
    return 1;
}

void read_snapshot(const char *snapshot_filename, FileMetadata *metadata_array, int *count_entries) {
    int snapshot_file = open(snapshot_filename, O_RDONLY);
    if (snapshot_file == -1) {
        if (errno != ENOENT) { 
            perror("open");
            exit(1);
        }
        *count_entries = 0;
        return; 
    }

    *count_entries = 0;
    char buffer[4096];
    while (read(snapshot_file, buffer, sizeof(buffer)) > 0) {
        sscanf(buffer, "Entry: %[^\n]\n", metadata_array[*count_entries].entry);
        sscanf(buffer, "Timestamp: %[^\n]\n", metadata_array[*count_entries].timestamp);
        sscanf(buffer, "Size: %ld bytes\n", &metadata_array[*count_entries].size);
        sscanf(buffer, "Last Modified: %[^\n]\n", metadata_array[*count_entries].last_modified);
        sscanf(buffer, "Permissions: %o\n", &metadata_array[*count_entries].permissions);
        sscanf(buffer, "Inode no: %ld\n", &metadata_array[*count_entries].inode_no);
        sscanf(buffer, "Type: %[^\n]\n", metadata_array[*count_entries].type);
        (*count_entries)++;
    }

    close(snapshot_file);
}

void write_snapshot(const char *snapshot_filename, const FileMetadata *metadata_array, int count_entries) {
    int snapshot_file = open(snapshot_filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if (snapshot_file == -1) {
        perror("open");
        exit(1);
    }

    for (int i = 0; i < count_entries; i++) {
        dprintf(snapshot_file, "Entry: %s\n", metadata_array[i].entry);
        dprintf(snapshot_file, "Timestamp: %s\n", metadata_array[i].timestamp);
        dprintf(snapshot_file, "Size: %ld bytes\n", metadata_array[i].size);
        dprintf(snapshot_file, "Last Modified: %s\n", metadata_array[i].last_modified);
        dprintf(snapshot_file, "Permissions: %o\n", metadata_array[i].permissions);
        dprintf(snapshot_file, "Inode no: %ld\n", metadata_array[i].inode_no);
        dprintf(snapshot_file, "Type: %s\n", metadata_array[i].type);
        dprintf(snapshot_file, "\n");
    }

    close(snapshot_file);
}

void traverse_directory(const char *dir_path, const char *output_dir, const char *isolated_space_dir, int pipe_write_fd, int *dangerous_files_count, int *count_directories) {
    DIR *dir;
    struct dirent *entry;
    struct stat file_info;
    char filepath[4096];
    struct tm *tm_info;

    dir = opendir(dir_path);
    if (dir == NULL) {
        perror("opendir");
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        sprintf(filepath, "%s/%s", dir_path, entry->d_name);

        if (stat(filepath, &file_info) == -1) {
            perror("stat");
            continue;
        }

        if ((file_info.st_mode & (S_IRWXU | S_IRWXG | S_IRWXO)) == 0) {
            int pipe_fds[2];
            if (pipe(pipe_fds) == -1) {
                perror("pipe");
                continue;
            }

            pid_t pid = fork();
            if (pid == -1) {
                perror("fork");
                continue;
            } else if (pid == 0) {
                close(pipe_fds[0]);  
                dup2(pipe_fds[1], STDOUT_FILENO); 
                close(pipe_fds[1]);  
                execl("/bin/bash", "bash", "/home/melisa/verify_for_malicious.sh", filepath, NULL);
                perror("execl");
                exit(EXIT_FAILURE);
            } else {
                close(pipe_fds[1]);  

                char buffer[4096];
                ssize_t num_read = read(pipe_fds[0], buffer, sizeof(buffer));
                if (num_read > 0) {
                    buffer[num_read - 1] = '\0'; 
                    write(pipe_write_fd, buffer, num_read);
                    (*dangerous_files_count)++; 
                }

                close(pipe_fds[0]);  
            }
        } else {
            tm_info = localtime(&file_info.st_mtime);

            FileMetadata new_meta;
            strcpy(new_meta.entry, entry->d_name);
            sprintf(new_meta.timestamp, "%d-%02d-%02d %02d:%02d:%02d",
                    tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
                    tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec);
            new_meta.size = file_info.st_size;
            sprintf(new_meta.last_modified, "%d-%02d-%02d %02d:%02d:%02d",
                    tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday,
                    tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec);
            new_meta.permissions = file_info.st_mode & 0777;
            new_meta.inode_no = file_info.st_ino;
            strcpy(new_meta.type, S_ISDIR(file_info.st_mode) ? "Directory" : "File");

            char snapshot_filename[4096];
            snprintf(snapshot_filename, sizeof(snapshot_filename), "%s/%s_snapshot.txt", output_dir, entry->d_name);

            FileMetadata old_meta;
            int count_entries = 0;
            read_snapshot(snapshot_filename, &old_meta, &count_entries);

            if (count_entries == 0 || !compare_metadata(&old_meta, &new_meta)) {
                write_snapshot(snapshot_filename, &new_meta, 1);
            }

            if (S_ISDIR(file_info.st_mode)) {
                traverse_directory(filepath, output_dir, isolated_space_dir, pipe_write_fd, dangerous_files_count, count_directories);
                (*count_directories)++;
            }
        }
    }

    closedir(dir);
}

int main(int argc, char *argv[]) {
    if (argc < 5 || argc > 15 || strcmp(argv[1], "-o") != 0 || strcmp(argv[3], "-s") != 0) {
        printf("Usage: %s -o <output_dir> -s <isolated_space_dir> <dir1> <dir2> ... <dirN>\n", argv[0]);
        return 1;
    }

    char *output_dir = argv[2];
    if (access(output_dir, F_OK) == -1) {
        printf("The output directory does not exist.\n");
        return 1;
    }

    char *isolated_space_dir = argv[4];
    if (access(isolated_space_dir, F_OK) == -1) {
        printf("The isolated space directory does not exist.\n");
        return 1;
    }

    int num_processes = argc - 5;

    int pipe_fds[2];
    if (pipe(pipe_fds) == -1) {
        perror("pipe");
        return 1;
    }

    int dangerous_files_count = 0;
    int count_directories = 0;
    for (int i = 5; i < argc; i++) {
        pid_t pid = fork();
        if (pid == -1) {
            perror("fork");
            exit(1);
        } else if (pid == 0) {
            close(pipe_fds[0]);  
            traverse_directory(argv[i], output_dir, isolated_space_dir, pipe_fds[1], &dangerous_files_count, &count_directories);
            close(pipe_fds[1]);  
            exit(dangerous_files_count); 
        } 
    }

    close(pipe_fds[1]);  

    char buffer[4096];
    ssize_t num_read;
    while ((num_read = read(pipe_fds[0], buffer, sizeof(buffer))) > 0) {
        pid_t child_pid = fork();
        if (child_pid == -1) {
            perror("fork");
            continue;
        } else if (child_pid == 0) {
            char corrupted_file[4096];
            strncpy(corrupted_file, buffer, num_read);
            corrupted_file[num_read - 1] = '\0'; 

            char *args[] = {"/bin/mv", corrupted_file, isolated_space_dir, NULL};
            execvp(args[0], args);
            perror("execvp");
            exit(EXIT_FAILURE);
        } else {
            printf("Moved corrupted file: %s\n", buffer);
        }
    }

    close(pipe_fds[0]);  

    int status;
    for (int i = 0; i < num_processes; i++) {
        pid_t child_pid = wait(&status);
        if (child_pid == -1) {
            perror("wait");
        } else {
            if (WIFEXITED(status)) {
                printf("Child process number %d with PID %d was terminated successfully with %d potentially dangerous files\n", i + 1, child_pid, WEXITSTATUS(status));
            } else {
                printf("Child process number %d with PID %d was NOT terminated successfully\n", i + 1, child_pid);
            }
        }
    }

    return 0;
}
