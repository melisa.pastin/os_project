#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void traverse_directory(const char *dir_path, FILE *snapshot_file, const char *snapshot_filename) {
    DIR *dir;
    struct dirent *entry;
    struct stat file_info;
    char file_path[4096];
    struct tm *tm_info;

    dir = opendir(dir_path);
    if (dir == NULL) {
        perror("opendir");
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0 || strcmp(entry->d_name, snapshot_filename) == 0)
            continue;  

        sprintf(file_path, "%s/%s", dir_path, entry->d_name);

        if (stat(file_path, &file_info) == -1) {
            perror("stat");
            continue;
        }

        tm_info = localtime(&file_info.st_mtime);

        fprintf(snapshot_file, "Entry: %s\n", entry->d_name);
        fprintf(snapshot_file, "Timestamp: %d-%02d-%02d %02d:%02d:%02d\n", 
                tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday, 
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec);
        fprintf(snapshot_file, "Size: %ld bytes\n", file_info.st_size);
        fprintf(snapshot_file, "Last Modified: %d-%02d-%02d %02d:%02d:%02d\n", 
                tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday, 
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec);
        fprintf(snapshot_file, "Permissions: %o\n", file_info.st_mode & 0777);
        fprintf(snapshot_file, "Inode no: %ld\n", file_info.st_ino);

        if (S_ISDIR(file_info.st_mode)) {
            fprintf(snapshot_file, "Type: Directory\n");
            traverse_directory(file_path, snapshot_file, snapshot_filename); 
        } else {
            fprintf(snapshot_file, "Type: File\n");
        }
        fprintf(snapshot_file, "\n");
    }

    closedir(dir);
}

int main(int argc, char *argv[]) 
{
    if (argc != 2) {
        printf("too many arguments\n", argv[0]);
        return 1;
    }

    char snapshot_file_path[4096];
    snprintf(snapshot_file_path, sizeof(snapshot_file_path), "%s/snapshot.txt", argv[1]);

    FILE *snapshot_file = fopen(snapshot_file_path, "w");
    if (snapshot_file == NULL) {
        perror("fopen");
        return 1;
    }

    traverse_directory(argv[1], snapshot_file, "snapshot.txt"); 

    fclose(snapshot_file);

    return 0; 
}
